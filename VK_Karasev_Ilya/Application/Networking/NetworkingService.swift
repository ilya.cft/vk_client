import Alamofire
import SwiftyJSON
import SwiftKeychainWrapper

class NetworkingService {
    
    let baseUrl = "https://api.vk.com"
    let token = KeychainWrapper.standard.string(forKey: "access_token") ?? ""
    
    //MARK: Session
    static let session: Session = {
        let config = URLSessionConfiguration.af.default
        config.timeoutIntervalForRequest = 60
        let session = Session(configuration: config)
        return session
    }()
    
    //MARK: API VK methods
    public func loadGroups(completion: ((Swift.Result<[Group], Error>) -> Void)? = nil) {
        let path = "/method/groups.get"
        
        let params: Parameters = [
            "access_token": token,
            "extended": "1",
            "v": "5.95"
        ]
        
        NetworkingService.session.request(baseUrl + path, method: .get, parameters: params).responseJSON { response in
            switch response.result  {
            case .success(let value):
                let json = JSON(value)
                let groups = json["response"]["items"].arrayValue.map { Group($0) }
                completion?(.success(groups))
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    public func findGroups(for searchText: String, completion: ((Swift.Result<[Group], Error>) -> Void)? = nil) {
        let path = "/method/groups.search"
        
        let params: Parameters = [
            "access_token": token,
            "q" : searchText,
            "type" : "group",
            "v" : "5.95"
        ]
        
        NetworkingService.session.request(baseUrl + path, method: .get, parameters: params).responseJSON { response in
            switch response.result  {
            case .success(let value):
                let json = JSON(value)
                let foundGroups = json["response"]["items"].arrayValue.map { Group($0) }
                completion?(.success(foundGroups))
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    public func loadFriends(completion: ((Swift.Result<[Friend], Error>) -> Void)? = nil) {
        let path = "/method/friends.get"
        
        let params: Parameters = [
            "access_token": token,
            "fields": "city, sex, domain, photo_100",
            "v": "5.95"
        ]
        
        NetworkingService.session.request(baseUrl + path, method: .get, parameters: params).responseJSON { response in
            switch response.result  {
            case .success(let value):
                let json = JSON(value)
                let friends = json["response"]["items"].arrayValue.map { Friend($0) }
                completion?(.success(friends))
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    public func loadUserAvatars(for userID: Int, completion: ((Swift.Result<[Photo], Error>) -> Void)? = nil) {
        let path = "/method/photos.get"
        
        let params: Parameters = [
            "access_token": token,
            "owner_id": userID,
            "album_id": "profile",
            "photo_sizes": "1",
            "rev": "1",
            "v": "5.95"
        ]
        
        NetworkingService.session.request(baseUrl + path, method: .get, parameters: params).responseJSON { response in
            switch response.result  {
            case .success(let value):
                let json = JSON(value)
                let photos = json["response"]["items"].arrayValue.map { Photo($0) }
                completion?(.success(photos))
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
}
