import SwiftyJSON

class Photo {
    var id: Int = 0
    var date = Date()
    var ownerId: Int = 0
    var smallImage: String = ""
    var middleImage: String = ""
    var largeImage: String = ""
    
    convenience init(_ json: JSON) {
        self.init()
        
        self.id = json["id"].intValue
        
        let date = json["date"].doubleValue
        self.date = Date(timeIntervalSince1970: date)
        
        self.ownerId = json["owner_id"].intValue
        self.smallImage = json["sizes"][2]["url"].stringValue
        self.middleImage = json["sizes"][8]["url"].stringValue
        self.largeImage = json["sizes"][6]["url"].stringValue
        
    }
}
