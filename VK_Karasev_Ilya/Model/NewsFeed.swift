import Foundation
import SwiftyJSON

class NewsFeedItem {
    
    var sourseID: Int = 0               //ID пользователя или группы
    var date = Date()                   //Дата публикации
    var text: String = ""               //Tекст новости
    var icon: String = ""               //Фото пользователя или группы
    var likes: Int = 0                  //Кол-во лайков
    var comments: Int = 0               //Кол-во комментариев
    var reposts: Int = 0                //Кол-во репостов
    var views: Int = 0                  //Кол-во просмотров
    
    convenience init(_ json: JSON) {
        self.init()
        
        self.sourseID = json["items"]["source_id"].intValue
        let date = json["items"]["date"].doubleValue
        self.date = Date(timeIntervalSince1970: date)
        self.text = json["items"]["text"].stringValue
        self.icon = json["items"]["attachments"][0]["photo"]["sizes"][3]["url"].stringValue
        self.likes = json["items"]["post_source"]["likes"]["count"].intValue
        self.comments = json["items"]["post_source"]["comments"]["count"].intValue
        self.reposts = json["items"]["post_source"]["reposts"]["count"].intValue
        self.views = json["items"]["post_source"]["views"]["count"].intValue
    }
}


