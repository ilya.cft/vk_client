import SwiftyJSON

class Friend {
    
    @objc enum Gender: Int {
        case male
        case female
        case unknown
    }
    
    @objc enum State: Int {
        case offline
        case online
    }
    
    var id: Int = 0
    var firstname: String = ""
    var lastname: String = ""
    var friendname: String = ""
    var avatar: String = ""
    var city: String = ""
    var state: State = .offline
    var gender: Gender = .male
    
    convenience init(_ json: JSON) {
        self.init()
    
        self.id = json["id"].intValue
        self.firstname = json["first_name"].stringValue
        self.lastname = json["last_name"].stringValue
        self.friendname = "\(firstname) \(lastname)"
        self.avatar = json["photo_100"].stringValue
        self.city = json["city"]["title"].stringValue
        self.state = Friend.stateFrom(json: json["online"].intValue)
        self.gender = Friend.genderFrom(json: json["sex"].intValue)
    }
    
    private static func stateFrom(json: Int) -> State {
        switch json {
        case 0: return .offline
        case 1: return .online
        default: fatalError()
        }
    }
    
    private static func genderFrom(json: Int) -> Gender {
        switch json {
        case 0: return .unknown
        case 1: return .female
        case 2: return .male
        default: fatalError()
        }
    }
}
