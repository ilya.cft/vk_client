import Foundation
import SwiftyJSON

class Group {
    var id: Int = 0
    var name: String = ""
    var icon: String = ""
    var privacy: Int = 0
    
    convenience init(_ json: JSON) {
        self.init()
        
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.icon = json["photo_100"].stringValue
        self.privacy = json["is_closed"].intValue
    }
}
