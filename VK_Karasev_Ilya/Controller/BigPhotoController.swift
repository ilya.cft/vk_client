import UIKit

class BigPhotoController: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var bigImageView: UIImageView!
    
    var currentImage: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bigImageView.image = currentImage
    }
}





