import UIKit

class FriendsPhotosController: UICollectionViewController {
    
    public var friendname = ""
    public var userID = Int()
    private let photoService = NetworkingService()
    private var friendphotos = [Photo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = friendname
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        photoService.loadUserAvatars(for: userID) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let photos):
                self.friendphotos = photos
                self.collectionView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    //MARK: CollectionView customization methods
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let width = collectionView.bounds.width / 3.1
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: width * 1.25)
        layout.sectionInset = UIEdgeInsets(top: 3.0, left: 3.0, bottom: 0.0, right: 3.0)
        layout.minimumInteritemSpacing = 2
        layout.minimumLineSpacing = 2
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    //MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friendphotos.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FriendsPhotosCell.reuseId, for: indexPath) as? FriendsPhotosCell
            else { fatalError() }
        
        let photo = friendphotos[indexPath.row]
        cell.configure(with: photo)
        cell.likeControl.addTarget(self, action: #selector(cellLikePressed(_:)), for: .valueChanged)
        return cell
    }

    //Mark - Private
    @objc func cellLikePressed(_ sender: LikeControl) {
        print("The cell liked status set to: \(sender.likeState).")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowBigPhoto",
            let bigPhotoVC = segue.destination as? BigPhotoController {
            let cell = sender as! FriendsPhotosCell
            let bigPhoto = cell.friendPhotoView.image
            bigPhotoVC.currentImage = bigPhoto
        }
    }
}







