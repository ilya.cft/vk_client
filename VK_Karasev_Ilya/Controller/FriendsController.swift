import UIKit

class FriendsController: UITableViewController, UISearchBarDelegate {
    
    private let friendsService = NetworkingService()
    private var friends = [Friend]()
    
    private var firstLetters = [Character]()
    private var sortedFriends = [Character: [Friend]]()
    private var searchedFriends = [Friend]()
    
    //MARK: Outlets
    @IBOutlet var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    
    //MARK: Controller lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        searchBar.placeholder = "Search Friends"
        definesPresentationContext = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        friendsService.loadFriends() { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let friends):
                self.friends = friends
                (self.firstLetters, self.sortedFriends) = self.sort(searchedFriends: friends)
                self.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: Sort & filter methods
    private func searchFriends (with text: String) {
        
        searchedFriends = friends.filter { friend in
            return friend.friendname.lowercased().contains(text.lowercased())
        }
    }
    
    internal func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            searchedFriends = Array(friends)
            (firstLetters, sortedFriends) = sort(searchedFriends: searchedFriends)
            tableView.reloadData()
            return
        }
        searchFriends(with: searchText)
        (firstLetters, sortedFriends) = sort(searchedFriends: searchedFriends)
        tableView.reloadData()
    }

    private func sort(searchedFriends: [Friend]) -> (firstLetters: [Character], sortedFriends: [Character: [Friend]]) {
        
        var firstLetters = [Character]()
        var sortedFriends = [Character: [Friend]]()
        
        for friend in searchedFriends {
            guard let firstLetter = friend.lastname.first else { continue }
            if  sortedFriends[firstLetter] != nil {
                sortedFriends[firstLetter]!.append(friend)
            } else {
                sortedFriends[firstLetter] = [friend]
            }
        }
        
        firstLetters = Array(sortedFriends.keys.sorted())
        return (firstLetters, sortedFriends)
    }
        
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return firstLetters.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let letter = firstLetters[section]
        guard let friends = sortedFriends[letter] else { return 0 }
        return friends.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FriendsCell.reuseId, for: indexPath) as? FriendsCell
            else { fatalError("Cell cannot be dequeued") }
        
        let letter = firstLetters[indexPath.section]
        guard let friends = sortedFriends[letter] else { return UITableViewCell() }
        
        let friend = friends[indexPath.row]
        cell.configure(with: friend)
        return cell
    }
    
    //MARK: Right side control
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return String(firstLetters[section])
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        
        var firstLettersString = [String]()
        for letter in firstLetters {
            let stringLetter = String(letter)
            firstLettersString.append(stringLetter)
        }
        return firstLettersString
    }
    
    // MARK: - Navigation methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowFriendPhotos",
        
            let friendphotosVC = segue.destination as? FriendsPhotosController,
            let indexPath = tableView.indexPathForSelectedRow {
            
            let letter = firstLetters[indexPath.section]
            let friends = sortedFriends[letter]!
            
            let friendname = "\(friends[indexPath.row].firstname) \(friends[indexPath.row].lastname)"
            friendphotosVC.friendname = friendname
            let userID = friends[indexPath.row].id
            friendphotosVC.userID = userID
        }
    }
}



