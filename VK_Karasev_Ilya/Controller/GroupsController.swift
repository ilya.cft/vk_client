import UIKit

class GroupsController: UITableViewController, UISearchBarDelegate {

    private let groupsService = NetworkingService()
    private var groups = [Group]()
    private var firstLetters = [Character]()
    private var sortedGroups = [Character: [Group]]()
    private var searchedGroups = [Group]()
    
    //MARK: Outlets
    @IBOutlet var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    
    //MARK: Controller lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        searchBar.placeholder = "Search Groups"
        definesPresentationContext = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        groupsService.loadGroups() { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let groups):
                self.groups = groups
                (self.firstLetters, self.sortedGroups) = self.sort(searchedGroups: groups)
                self.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: Sort & filter methods
    private func searchGroups (with text: String) {
        
        searchedGroups = groups.filter { group in
            return group.name.lowercased().contains(text.lowercased())
        }
    }
        
    internal func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            searchedGroups = Array(groups)
            (firstLetters, sortedGroups) = sort(searchedGroups: searchedGroups)
            tableView.reloadData()
            return
        }
        searchGroups(with: searchText)
        (firstLetters, sortedGroups) = sort(searchedGroups: searchedGroups)
        tableView.reloadData()
    }
    
    private func sort(searchedGroups: [Group]) -> (firstLetters: [Character], sortedFGroups: [Character:[Group]]) {
        
        var firstLetters = [Character]()
        var sortedGroups = [Character: [Group]]()
        
        for group in searchedGroups {
            guard let firstLetter = group.name.first else { continue }
            if  sortedGroups[firstLetter] != nil {
                sortedGroups[firstLetter]!.append(group)
            } else {
                sortedGroups[firstLetter] = [group]
            }
        }
        
        firstLetters = Array(sortedGroups.keys.sorted())
        return (firstLetters, sortedGroups)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return firstLetters.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let letter = firstLetters[section]
        guard let groups = sortedGroups[letter] else { return 0 }
        return groups.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: GroupsCell.reuseId, for: indexPath) as? GroupsCell
            else { fatalError("Cell cannot be dequeued") }
        
        let letter = firstLetters[indexPath.section]
        guard let groups = sortedGroups[letter] else { return UITableViewCell() }
        
        let group = groups[indexPath.row]
        cell.configure(with: group)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return String(firstLetters[section])
    }
}

