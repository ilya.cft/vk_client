import UIKit
import Kingfisher

class FriendsPhotosCell: UICollectionViewCell {
    
    static let reuseId = "FriendsPhotosCell"
    
    @IBOutlet var friendPhotoView: UIImageView!
    
    @IBOutlet weak var likeControl: LikeControl!
    
    public func configure(with photo: Photo) {
  
        var iconUrlString = photo.middleImage
        if iconUrlString == "" {
            iconUrlString = photo.smallImage
            friendPhotoView.kf.setImage(with: URL(string: iconUrlString))
        } else {
            iconUrlString = photo.middleImage
            friendPhotoView.kf.setImage(with: URL(string: iconUrlString))
        }
    }
}
